
try {
   const sortCarModelsAlphabetically = require ('../Problem3.js')
   let inventory = require('../inventory.js')
   
   const sortedModels = sortCarModelsAlphabetically(inventory);
   
   console.log(sortedModels);
   
   }catch(error) {

       console.error('Error:', error.message);
       
   }