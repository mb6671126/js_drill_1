try {
    const getCarYears = require('../Problem4.js');
    
    const inventory = require('../inventory.js');
    const years = getCarYears(inventory);
    
    console.log(years);
    }catch (error){
        console.error('Error:', error.message);
    }