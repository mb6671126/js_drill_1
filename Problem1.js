// // ==== Problem #1 ====
// Function to find car by id
function findCarById(inventory, id) {
  try {
      
      // Check if id is provided
      if (id === undefined || id === null) {
          throw new Error('ID is required');
      }

      for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === id) {
           return inventory[i];
        }
      }
  return null;
  } catch (error) {
      console.error('Error in findCarById function:', error.message);
      return null;
  }
}

module.exports = findCarById;